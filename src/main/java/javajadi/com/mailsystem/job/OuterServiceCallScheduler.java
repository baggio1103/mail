package javajadi.com.mailsystem.job;

import com.fasterxml.jackson.core.JsonProcessingException;
import javajadi.com.mailsystem.outerservice.OuterServiceCall;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class OuterServiceCallScheduler {

    private final OuterServiceCall serviceCall;

    @Scheduled(cron = "${cron.everyTwoMinutes}")
    public void sendRequests() throws JsonProcessingException {
        log.info("Sending request to outer service");
        serviceCall.callOuterService();
    }

}
