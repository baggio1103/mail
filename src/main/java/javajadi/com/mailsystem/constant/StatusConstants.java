package javajadi.com.mailsystem.constant;

public class StatusConstants {

    public static final String READY_TO_SENT = "Message ready to be mailed";

    public static final String SENT = "Mailed";

}
