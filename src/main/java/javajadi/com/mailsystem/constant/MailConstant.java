package javajadi.com.mailsystem.constant;

public class MailConstant {

    public static final String MAIL_NOT_FOUND = "Mail not found by id: ";

    public static final String MAIL_DELETED = "Mail deleted successfully ";

}
