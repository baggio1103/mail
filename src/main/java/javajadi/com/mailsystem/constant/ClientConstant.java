package javajadi.com.mailsystem.constant;

public class ClientConstant {

    public static final String CLIENT_NOT_FOUND = "Client not found by id: ";

    public static final String CLIENT_DELETED = "Client deleted successfully ";

}
