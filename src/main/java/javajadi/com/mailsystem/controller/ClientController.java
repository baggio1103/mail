package javajadi.com.mailsystem.controller;

import javajadi.com.mailsystem.constant.ClientConstant;
import javajadi.com.mailsystem.data.ClientData;
import javajadi.com.mailsystem.data.HttpResponse;
import javajadi.com.mailsystem.exception.ExceptionHandling;
import javajadi.com.mailsystem.service.ClientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static javajadi.com.mailsystem.constant.ClientConstant.CLIENT_DELETED;
import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/clients")
public class ClientController extends ExceptionHandling {

    private final ClientService clientService;

    @GetMapping("")
    public List<ClientData> findAllClientData() {
        return clientService.findAllClients();
    }

    @PostMapping("")
    public ClientData saveClientData(@RequestBody ClientData clientData) {
        return clientService.saveClient(clientData);
    }

    @PutMapping("/{clientId}")
    public ClientData updateClientData(@PathVariable("clientId") String clientId, @RequestBody ClientData clientData) {
        return clientService.updateClientData(clientId, clientData);
    }

    @DeleteMapping("/{clientId}")
    public ResponseEntity<HttpResponse> deleteClient(@PathVariable("clientId") String clientId) {
        clientService.deleteClientById(clientId);
        return createHttpResponse(OK, CLIENT_DELETED);
    }

}
