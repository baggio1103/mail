package javajadi.com.mailsystem.controller;

import javajadi.com.mailsystem.data.HttpResponse;
import javajadi.com.mailsystem.data.MailData;
import javajadi.com.mailsystem.exception.ExceptionHandling;
import javajadi.com.mailsystem.service.MailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static javajadi.com.mailsystem.constant.MailConstant.MAIL_DELETED;
import static org.springframework.http.HttpStatus.OK;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/mails")
public class MailController extends ExceptionHandling {

    private final MailService mailService;

    @PostMapping("")
    public MailData saveMail(@RequestBody MailData mailData) {
        return mailService.saveMail(mailData);
    }

    @GetMapping("")
    public List<MailData> findAllMails() {
        return mailService.findAllMails();
    }

    @PutMapping("/{mailId}")
    public MailData updateMail(@PathVariable("mailId") String mailId, @RequestBody MailData mailData) {
        return mailService.updateMail(mailId, mailData);
    }

    @DeleteMapping("/{mailId}")
    public ResponseEntity<HttpResponse> deleteMail(@PathVariable("mailId") String mailId) {
        mailService.deleteMail(mailId);
        return createHttpResponse(OK, MAIL_DELETED);
    }

}
