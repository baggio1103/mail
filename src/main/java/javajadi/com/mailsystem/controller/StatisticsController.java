package javajadi.com.mailsystem.controller;

import javajadi.com.mailsystem.data.StatisticsData;
import javajadi.com.mailsystem.service.StatisticsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/statistics")
public class StatisticsController {

    private final StatisticsService statisticsService;

    @GetMapping("")
    public List<StatisticsData> getStatistics() {
        return statisticsService.getStatistics();
    }

}
