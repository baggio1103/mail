package javajadi.com.mailsystem.service;

import javajadi.com.mailsystem.data.MailData;

import java.util.List;

public interface MailService {

    MailData saveMail(MailData mailData);

    List<MailData> findAllMails();

    MailData updateMail(String mailId, MailData mailData1);

    void deleteMail(String mailId);

}
