package javajadi.com.mailsystem.service;

import javajadi.com.mailsystem.data.StatisticsData;

import java.util.List;

public interface StatisticsService {

    List<StatisticsData> getStatistics();

}
