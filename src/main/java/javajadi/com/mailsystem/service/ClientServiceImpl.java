package javajadi.com.mailsystem.service;

import javajadi.com.mailsystem.data.ClientData;
import javajadi.com.mailsystem.domain.Client;
import javajadi.com.mailsystem.domain.Tag;
import javajadi.com.mailsystem.exception.domain.ClientNotFoundException;
import javajadi.com.mailsystem.mapper.ClientMapper;
import javajadi.com.mailsystem.repository.ClientRepository;
import javajadi.com.mailsystem.repository.TagRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.time.LocalDateTime.now;
import static javajadi.com.mailsystem.constant.ClientConstant.CLIENT_NOT_FOUND;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    private final TagRepository tagRepository;

    private final ClientMapper clientMapper;

    @Override
    public ClientData saveClient(ClientData clientData) {
        Client client = clientMapper.toClient(clientData, generateClientId());
        List<String> tags = tagRepository.findAllByTagIsIn(clientData.getMailTags()).stream()
                .peek(tag -> {
                    client.addTag(tag);
                    tag.getMails().forEach(mail -> {
                        if (!mail.getMailingExpirationTime().isAfter(now())) {
                            mail.getMessage().addClient(client);
                        }
                    });
                })
                .map(Tag::getTag).collect(Collectors.toList());
        clientData.getMailTags().stream().filter(mailTag -> !tags.contains(mailTag))
                .forEach(mailTag -> constructTag(client, mailTag));
        clientRepository.save(client);
        return clientMapper.toClientData(client);
    }

    @Override
    public List<ClientData> findAllClients() {
        return clientMapper.toClientData(clientRepository.findAll());
    }

    @Override
    public ClientData updateClientData(String clientId, ClientData clientData) {
        Client client = clientRepository.findByClientId(clientId)
                .orElseThrow(() -> new ClientNotFoundException(CLIENT_NOT_FOUND + clientId));
        client.setCode(clientData.getCode());
        client.setPhone(clientData.getPhone());
        client.setTimeZone(clientData.getTimeZone());
        List<Tag> tags = client.getTags();
        List<Tag> clientTags = new ArrayList<>(tags);
        clientTags.forEach(tag -> {
            if (!clientData.getMailTags().contains(tag.getTag())) {
                client.removeTag(tag);
                tag.getMails().forEach(mail -> mail.getMessage().removeClient(client));
            }
        });
        clientData.getMailTags().stream().filter(mailTag -> clientTags.stream()
                .noneMatch(tag -> tag.getTag().equalsIgnoreCase(mailTag)))
                .forEach(mailTag -> {
                    Optional<Tag> optionalTag = tagRepository.findTagByTag(mailTag);
                    if (optionalTag.isPresent()) {
                        client.addTag(optionalTag.get());
                        optionalTag.get().getMails().forEach(mail -> mail.getMessage().addClient(client));
                    } else {
                        constructTag(client, mailTag);
                    }
                }
        );
        clientRepository.save(client);
        return clientMapper.toClientData(client);
    }

    @Override
    public void deleteClientById(String clientId) {
        Client client = clientRepository.findByClientId(clientId)
                .orElseThrow(() -> new ClientNotFoundException(CLIENT_NOT_FOUND + clientId));
//        client.clearAllData();
        clientRepository.delete(client);
    }

    private Tag constructTag(Client client, String mailTag) {
        Tag tag = new Tag();
        tag.setTag(mailTag);
        client.addTag(tag);
        return tag;
    }

    private String generateClientId() {
        return randomAlphanumeric(7) + clientRepository.count();
    }

}
