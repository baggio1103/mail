package javajadi.com.mailsystem.service;

import javajadi.com.mailsystem.data.StatisticsData;
import javajadi.com.mailsystem.mapper.StatisticsMapper;
import javajadi.com.mailsystem.repository.StatisticsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class StatisticsServiceImpl implements StatisticsService {

    private final StatisticsMapper statisticsMapper;

    private final StatisticsRepository statisticsRepository;

    @Override
    public List<StatisticsData> getStatistics() {
        return statisticsMapper.mapStatistics(statisticsRepository.findAll());
    }

}
