package javajadi.com.mailsystem.service;

import javajadi.com.mailsystem.data.ClientData;

import java.util.List;

public interface ClientService {

    ClientData saveClient(ClientData clientData);

    List<ClientData> findAllClients();

    ClientData updateClientData(String clientId, ClientData clientData);

    void deleteClientById(String clientId);

}
