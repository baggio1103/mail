package javajadi.com.mailsystem.service;

import javajadi.com.mailsystem.data.MailData;
import javajadi.com.mailsystem.domain.Client;
import javajadi.com.mailsystem.domain.Mail;
import javajadi.com.mailsystem.domain.Message;
import javajadi.com.mailsystem.domain.Tag;
import javajadi.com.mailsystem.exception.domain.MailNotFoundException;
import javajadi.com.mailsystem.mapper.MailMapper;
import javajadi.com.mailsystem.repository.ClientRepository;
import javajadi.com.mailsystem.repository.MailRepository;
import javajadi.com.mailsystem.repository.TagRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static javajadi.com.mailsystem.constant.MailConstant.MAIL_NOT_FOUND;
import static javajadi.com.mailsystem.constant.StatusConstants.READY_TO_SENT;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

    private final MailRepository mailRepository;

    private final TagRepository tagRepository;

    private final ClientRepository clientRepository;

    private final MailMapper mailMapper;

    @Override
    public MailData saveMail(MailData mailData) {
        Mail mail = mailMapper.toMail(mailData);
        List<String> tags = tagRepository.findAllByTagIsIn(mailData.getMailTags()).stream()
                .peek(mail::addTag).map(Tag::getTag).collect(Collectors.toList());
        mailData.getMailTags().stream().filter(mailTag -> !tags.contains(mailTag))
                .forEach(mailTag -> constructTag(mail, mailTag));
        mail.setMailId(generateMailId());
        Message message = constructMessage(mailData);
        mail.setMessage(message);
        mailRepository.save(mail);
        return mailMapper.toMailData(mail);
    }

    @Override
    public List<MailData> findAllMails() {
        return mailMapper.mapMails(mailRepository.findAll());
    }

    @Override
    public MailData updateMail(String mailId, MailData mailData) {
        Mail mail = mailRepository.findByMailId(mailId)
                .orElseThrow(() -> new MailNotFoundException(MAIL_NOT_FOUND + mailId));
        mail.setMailingTime(mailData.getMailingTime());
        mail.setMailingExpirationTime(mailData.getMailingExpirationTime());
        List<Tag> mailTags = mail.getTags();
        ArrayList<Tag> tags = new ArrayList<>(mailTags);
        Message message = mail.getMessage();
        tags.forEach(tag -> {
            if (!mailData.getMailTags().contains(tag.getTag())) {
                mail.removeTag(tag);
                ArrayList<Client> clients = new ArrayList<>(message.getClients());
                clients.forEach(client -> {
                    if (client.getTags().contains(tag)) {
                        message.removeClient(client);
                    }
                });
            }
        });
        mailData.getMailTags().stream().filter(mailTag -> mailTags.stream().
                noneMatch(tag -> tag.getTag().equals(mailTag)))
                .forEach(mailTag -> {
                    Optional<Tag> optionalTag = tagRepository.findTagByTag(mailTag);
                    if (optionalTag.isPresent()) {
                        mail.addTag(optionalTag.get());
                    } else {
                        constructTag(mail, mailTag);
                    }
                });
        message.setMessage(mailData.getMailText());
        clientRepository.findAllClientsWithTagsIn(mailData.getMailTags()).forEach(message::addClient);
        mailRepository.save(mail);
        return mailMapper.toMailData(mail);
    }

    @Override
    public void deleteMail(String mailId) {
        Mail mail = mailRepository.findByMailId(mailId)
                .orElseThrow(() -> new MailNotFoundException(MAIL_NOT_FOUND + mailId));
        mailRepository.delete(mail);
    }

    private Message constructMessage(MailData mailData) {
        Message message = new Message();
        message.setMessage(mailData.getMailText());
        message.setStatus(READY_TO_SENT);
        clientRepository.findAllClientsWithTagsIn(mailData.getMailTags()).forEach(message::addClient);
        return message;
    }

    private void constructTag(Mail mail, String mailTag) {
        Tag tag = new Tag();
        tag.setTag(mailTag);
        mail.addTag(tag);
    }

    private String generateMailId() {
        return randomAlphanumeric(7) + mailRepository.count();
    }

}
