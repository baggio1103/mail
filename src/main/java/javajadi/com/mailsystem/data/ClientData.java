package javajadi.com.mailsystem.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientData {

    private String clientId;

    private String phone;

    private String code;

    private List<String> mailTags;

    private String timeZone;

}
