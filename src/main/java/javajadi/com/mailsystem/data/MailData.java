package javajadi.com.mailsystem.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MailData {

    private String mailId;

    private LocalDateTime mailingTime;

    private String mailText;

    private List<String> mailTags;

    private LocalDateTime mailingExpirationTime;

}
