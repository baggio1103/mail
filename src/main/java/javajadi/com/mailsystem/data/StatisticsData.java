package javajadi.com.mailsystem.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

import static javajadi.com.mailsystem.constant.TimeFormat.DATE_TIME_FORMAT;

@Getter
@Setter
@NoArgsConstructor
public class StatisticsData {

    private String tags;

    private Integer mailsCount;

    @JsonFormat(pattern = DATE_TIME_FORMAT)
    private LocalDateTime sentDate;
}
