package javajadi.com.mailsystem.repository.dao;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class MailDao {

    private static final String sql = "SELECT c.client_id, c.phone, mes.message, m.mail_id from mail m " +
            "inner join message mes on m.id = mes.mail_id\n" +
            "inner join client_messages cm on mes.mail_id = cm.message_id\n" +
            "inner join client c on cm.client_id = c.id\n" +
            "where now() <= m.mailing_expiration_time and m.is_mailed = 'false'";

    private final JdbcTemplate jdbcTemplate;

    public List<ClientDao> getAllClientMails() {
        return jdbcTemplate.query(sql, new ClientDaoMapper());
    }

}
