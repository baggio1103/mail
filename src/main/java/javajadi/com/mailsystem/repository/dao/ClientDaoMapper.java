package javajadi.com.mailsystem.repository.dao;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ClientDaoMapper implements RowMapper<ClientDao> {

    @Override
    public ClientDao mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new ClientDao(
                rs.getString("client_id"),
                rs.getString("phone"),
                rs.getString("message"),
                rs.getString("mail_id")
        );
    }

}
