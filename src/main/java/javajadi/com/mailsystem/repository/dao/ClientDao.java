package javajadi.com.mailsystem.repository.dao;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ClientDao {

    private String id;

    private String phone;

    private String code;

    private String mailId;

}
