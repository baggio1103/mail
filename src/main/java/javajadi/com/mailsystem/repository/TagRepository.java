package javajadi.com.mailsystem.repository;

import javajadi.com.mailsystem.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TagRepository extends JpaRepository<Tag, Long> {

    Optional<Tag> findTagByTag(String tag);

    List<Tag> findAllByTagIsIn(List<String> tag);

}
