package javajadi.com.mailsystem.repository;

import javajadi.com.mailsystem.domain.Mail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MailRepository extends JpaRepository<Mail, Long> {

    Optional<Mail> findByMailId(String mailId);

    List<Mail> findAllByMailIdIn(List<String> mailIds);

}
