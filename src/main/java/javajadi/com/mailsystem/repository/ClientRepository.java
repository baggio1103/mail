package javajadi.com.mailsystem.repository;

import javajadi.com.mailsystem.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {

    Optional<Client> findByClientId(String clientId);

    @Query("SELECT DISTINCT client FROM Client client" +
            " JOIN client.tags tags " +
            "WHERE tags.tag IN (:tags)")
    List<Client> findAllClientsWithTagsIn(@Param("tags") List<String> tags);

}
