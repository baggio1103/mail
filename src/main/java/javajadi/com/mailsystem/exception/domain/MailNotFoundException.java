package javajadi.com.mailsystem.exception.domain;

public class MailNotFoundException extends RuntimeException {

    public MailNotFoundException(String message) {
        super(message);
    }

}
