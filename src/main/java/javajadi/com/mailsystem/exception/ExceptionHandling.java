package javajadi.com.mailsystem.exception;

import javajadi.com.mailsystem.data.HttpResponse;
import javajadi.com.mailsystem.exception.domain.ClientNotFoundException;
import javajadi.com.mailsystem.exception.domain.MailNotFoundException;
import javajadi.com.mailsystem.util.ResponseHandler;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestControllerAdvice
public class ExceptionHandling extends ResponseHandler {


    @ExceptionHandler(MailNotFoundException.class)
    public ResponseEntity<HttpResponse> mailNotFoundException(MailNotFoundException exception){
        return createHttpResponse(BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(ClientNotFoundException.class)
    public ResponseEntity<HttpResponse> clientNotFoundException(ClientNotFoundException exception){
        return createHttpResponse(BAD_REQUEST, exception.getMessage());
    }

}
