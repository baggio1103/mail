package javajadi.com.mailsystem.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static org.springframework.util.StreamUtils.copyToString;

@Slf4j
public class RequestResponseLoggingInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {
        logRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);
        logResponse(request, response);
        return response;
    }

    private void logRequest(HttpRequest request, byte[] body) {
        log.info("{} |> {} body: {}", request.getURI(), request.getMethod(),
                new String(body, StandardCharsets.UTF_8));
    }

    private void logResponse(HttpRequest request, ClientHttpResponse response) throws IOException {
        if ("attachment".equals(response.getHeaders().getContentDisposition().getType())) {
            log.info("{} |< status: {} {} body is file", request.getURI(), response.getStatusCode(),
                    response.getStatusText());
        } else {
            log.info("{} |< status: {} {} body: {}", request.getURI(), response.getStatusCode(),
                    response.getStatusText(),
                    copyToString(response.getBody(), Charset.defaultCharset()));
        }
    }

}
