package javajadi.com.mailsystem.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

@Slf4j
public class CustomRestHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        return clientHttpResponse.getStatusCode().series() == CLIENT_ERROR
                || clientHttpResponse.getStatusCode().series() == SERVER_ERROR;
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
        InputStream body = clientHttpResponse.getBody();
        String responseBodyAsString = IOUtils.toString(body, StandardCharsets.UTF_8.name());
        if (clientHttpResponse.getStatusCode().series() == SERVER_ERROR) {
            log.error("Custom SERVER_ERROR: message: {}", responseBodyAsString);
            throw new HttpClientErrorException(clientHttpResponse.getStatusCode());
        } else if (clientHttpResponse.getStatusCode().series() == CLIENT_ERROR) {
            log.error("Custom CLIENT_ERROR: status: {} message: {}", clientHttpResponse.getStatusText(), responseBodyAsString);
            if (clientHttpResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new RuntimeException(responseBodyAsString);
            }
        }
    }
}
