package javajadi.com.mailsystem.mapper;

import javajadi.com.mailsystem.outerservice.request.ClientMail;
import javajadi.com.mailsystem.repository.dao.ClientDao;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ClientMailMapper {

    ClientMail toClientMail(ClientDao clientDao);

}
