package javajadi.com.mailsystem.mapper;

import javajadi.com.mailsystem.data.StatisticsData;
import javajadi.com.mailsystem.domain.Statistics;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface StatisticsMapper {

    StatisticsData toStatisticsData(Statistics statistics);

    default List<StatisticsData> mapStatistics(List<Statistics> statisticsList) {
        return statisticsList.stream().map(this::toStatisticsData).collect(Collectors.toList());
    }

}
