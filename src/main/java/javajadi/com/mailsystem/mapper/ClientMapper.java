package javajadi.com.mailsystem.mapper;

import javajadi.com.mailsystem.data.ClientData;
import javajadi.com.mailsystem.domain.Client;
import javajadi.com.mailsystem.domain.Tag;
import jdk.jfr.Name;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    @Mappings({
            @Mapping(source = "tags", target = "mailTags", qualifiedByName = "mapTags")
    })
    ClientData toClientData(Client client);

    @Mappings({
            @Mapping(source = "clientId", target = "clientId")
    })
    Client toClient(ClientData clientData, String clientId);

    @Named("mapTags")
    default List<String> mapTags(List<Tag> tags) {
        return tags.stream().map(Tag::getTag).collect(Collectors.toList());
    }

    default List<ClientData> toClientData(List<Client> clients) {
        return clients.stream().map(this::toClientData).collect(Collectors.toList());
    }

}
