package javajadi.com.mailsystem.mapper;

import javajadi.com.mailsystem.data.MailData;
import javajadi.com.mailsystem.domain.Mail;
import javajadi.com.mailsystem.domain.Message;
import javajadi.com.mailsystem.domain.Tag;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.Objects.nonNull;
import static org.springframework.util.CollectionUtils.isEmpty;

@Mapper(componentModel = "spring")
public interface MailMapper {

    Mail toMail(MailData mailData);

    @Mappings({
            @Mapping(source = "tags", target = "mailTags", qualifiedByName = "mapTags"),
            @Mapping(source = "message", target = "mailText", qualifiedByName = "mapMessage")
    })
    MailData toMailData(Mail mail);

    @Named("mapTags")
    default List<String> mapTags(List<Tag> tags) {
        return isEmpty(tags) ? emptyList()
                : tags.stream().map(Tag::getTag).collect(Collectors.toList());
    }

    default List<MailData> mapMails(List<Mail> mails) {
        return mails.stream().map(this::toMailData).collect(Collectors.toList());
    }

    @Named("mapMessage")
    default String mapMailMessage(Message message) {
        return nonNull(message) ? message.getMessage() : "";
    }

}
