package javajadi.com.mailsystem.outerservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javajadi.com.mailsystem.domain.Mail;
import javajadi.com.mailsystem.domain.Statistics;
import javajadi.com.mailsystem.domain.Tag;
import javajadi.com.mailsystem.mapper.ClientMailMapper;
import javajadi.com.mailsystem.outerservice.request.ClientMail;
import javajadi.com.mailsystem.outerservice.response.ApiResponse;
import javajadi.com.mailsystem.repository.MailRepository;
import javajadi.com.mailsystem.repository.StatisticsRepository;
import javajadi.com.mailsystem.repository.dao.ClientDao;
import javajadi.com.mailsystem.repository.dao.MailDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static javajadi.com.mailsystem.constant.StatusConstants.SENT;

@Slf4j
@Service
@RequiredArgsConstructor
public class OuterServiceCallImpl implements OuterServiceCall {

    private final MailDao mailDao;

    private final RestTemplate restTemplate;

    private final ObjectMapper mapper;

    private final MailRepository mailRepository;

    private final StatisticsRepository statisticsRepository;

    private final HttpHeaders headers;

    private final ClientMailMapper clientMailMapper;

    @Value("${outer-service.url}")
    private String url;

    @Override
    @Transactional
    public void callOuterService() throws JsonProcessingException {
        List<ClientDao> clientDaoList = mailDao.getAllClientMails();
        log.info("Clients to be mailed: {}", mapper.writeValueAsString(clientDaoList));
        List<String> mailIds = clientDaoList.stream()
                .map(ClientDao::getMailId).collect(Collectors.toList());
        sendOuterServiceRequest(clientDaoList);
        List<Mail> mails = mailRepository.findAllByMailIdIn(mailIds);
        List<String> tags = mails.stream()
                .flatMap(mail -> mail.getTags().stream()
                        .map(Tag::getTag)).collect(Collectors.toList());
        mails.forEach(mail -> {
            mail.setIsMailed(true);
            mail.getMessage().setSendingTime(LocalDateTime.now());
            mail.getMessage().setStatus(SENT);
        });
        mailRepository.saveAll(mails);
        if (!clientDaoList.isEmpty()) {
            String tagString = tags.toString().replace("[", "").replace("]", "");
            statisticsRepository.save(new Statistics(tagString, clientDaoList.size()));
        }
    }

    private void sendOuterServiceRequest(List<ClientDao> clientDaoList) {
        List<ClientMail> clientMails = clientDaoList.stream()
                .map(clientMailMapper::toClientMail).collect(Collectors.toList());
        clientMails.forEach(this::call);
    }

    @Retryable(include = {RestClientException.class, RuntimeException.class}, maxAttempts = 5,
            backoff = @Backoff(delay = 100))
    public void call(ClientMail clientMail) {
        HttpEntity<ClientMail> httpEntity = new HttpEntity<>(clientMail, headers);
        restTemplate
                .exchange(url, HttpMethod.POST, httpEntity, ApiResponse.class, clientMail.getId());
    }

    @Recover
    void recover(RestClientException exception, List<ClientDao> clientDaoList) throws JsonProcessingException {
        log.info("Exception happened while sending request to {}, exception message = {}", url, exception.getMessage());
        log.info("Request Body of the request - {}", mapper.writeValueAsString(clientDaoList));
    }

}
