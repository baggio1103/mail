package javajadi.com.mailsystem.outerservice;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface OuterServiceCall {

    void callOuterService() throws JsonProcessingException;

}
