package javajadi.com.mailsystem.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

@Getter
@Setter
@Entity
@Table(name = "client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NaturalId
    private String clientId;

    private String phone;

    private String code;

    @ManyToMany(cascade = {PERSIST, MERGE}, fetch = FetchType.LAZY)
    @JoinTable(name = "client_tags",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tags;

    @Column(name = "time_zone", columnDefinition = "varchar(25)")
    private String timeZone;

    @ManyToMany(cascade = {PERSIST, MERGE})
    @JoinTable(name = "client_messages",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "message_id"))
    private List<Message> messages;

    public Client() {
        tags = new ArrayList<>();
        messages = new ArrayList<>();
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }

    public void removeTag(Tag tag) {
        tags.remove(tag);
    }

}
