package javajadi.com.mailsystem.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

@Getter
@Setter
@Entity
@Table(name = "message")
public class Message {

    @Id
    private Long id;

    @Column(columnDefinition = "TEXT not null")
    private String message;

    private LocalDateTime sendingTime;

    private String status;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    private Mail mail;

    @ManyToMany(cascade = {PERSIST, MERGE})
    @JoinTable(name = "client_messages",
            joinColumns = @JoinColumn(name = "message_id"),
            inverseJoinColumns = @JoinColumn(name = "client_id"))
    private List<Client> clients;

    public Message() {
        clients = new ArrayList<>();
    }

    public void addClient(Client client) {
        clients.add(client);
    }

    public void removeClient(Client client) {
        clients.remove(client);
    }

}

