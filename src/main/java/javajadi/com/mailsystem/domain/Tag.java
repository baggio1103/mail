package javajadi.com.mailsystem.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "tag")
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String tag;

    @ManyToMany(mappedBy = "tags")
    private List<Mail> mails;

    @ManyToMany(mappedBy = "tags")
    private List<Client> clients;

    public Tag() {
        mails = new ArrayList<>();
        clients = new ArrayList<>();
    }

}
