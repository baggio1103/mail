package javajadi.com.mailsystem.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.*;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Table(name = "mail")
public class Mail {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @NaturalId
    private String mailId;

    private LocalDateTime mailingTime;

    @Column(name = "is_mailed", columnDefinition = " boolean default false")
    private Boolean isMailed;

    @OneToOne(mappedBy = "mail", cascade = ALL, fetch = LAZY)
    private Message message;

    @ManyToMany(cascade = {MERGE, PERSIST}, fetch = LAZY)
    @JoinTable(name = "mail_tags",
            joinColumns = @JoinColumn(name = "mail_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tags;

    private LocalDateTime mailingExpirationTime;

    public Mail() {
        tags = new ArrayList<>();
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }

    public void removeTag(Tag tag) {
        tags.remove(tag);
    }

    public void setMessage(Message message) {
        this.message = message;
        message.setMail(this);
    }

}
