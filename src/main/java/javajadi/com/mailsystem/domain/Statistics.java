package javajadi.com.mailsystem.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "statistics")
public class Statistics {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tags", columnDefinition = "TEXT", nullable = false)
    private String tags;

    @Column(name = "mails_count")
    private Integer mailsCount;

    @CreationTimestamp
    private LocalDateTime sentDate;

    public Statistics(String tags, Integer mailsCount) {
        this.tags = tags;
        this.mailsCount = mailsCount;
    }
}
