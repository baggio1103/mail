FROM openjdk:11
WORKDIR /app
COPY /target/mail.jar .
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "mail.jar"]